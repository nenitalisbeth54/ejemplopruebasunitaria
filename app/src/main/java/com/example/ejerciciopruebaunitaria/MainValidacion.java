package com.example.ejerciciopruebaunitaria;

public class MainValidacion {
    public boolean validacionCampo(String cadena) {
        if (cadena.length()== 0){
            return false;
        }
        return true;
    }

    public boolean validacionLargoUno(String cadena) {
        if (cadena.length() >=5 ){
            return true;
        }
        return false;
    }
    public boolean validacionLargoDos(String cadena) {
        if (cadena.length() >= 10){
            return true;
        }
        return false;
    }
}
