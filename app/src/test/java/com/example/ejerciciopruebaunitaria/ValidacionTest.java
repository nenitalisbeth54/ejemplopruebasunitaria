package com.example.ejerciciopruebaunitaria;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ValidacionTest {
    @Test
    public void validacionCampo_True() {
        MainValidacion mainValidacion = new MainValidacion();
        assertTrue(mainValidacion.validacionCampo("Nombre"));
    }
    @Test
    public void validacionLargoUno_True() {
        MainValidacion mainValidacion = new MainValidacion();
        assertTrue(mainValidacion.validacionLargoUno("Nombr"));
    }
    @Test
    public void validacionLargoDos_True() {
        MainValidacion mainValidacion = new MainValidacion();
        assertTrue(mainValidacion.validacionLargoDos("NombreName"));
    }
}
